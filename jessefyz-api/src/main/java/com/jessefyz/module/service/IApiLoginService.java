package com.jessefyz.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.req.MiniAppLoginReq;
import com.jessefyz.module.vo.MemberVo;

/**
 * 登录service
 *
 */
public interface IApiLoginService extends IService<MemberInfo> {

    AjaxResult<MemberVo> loginBywechatMiniApp(MiniAppLoginReq miniAppLoginReq)  throws Exception ;

    MemberVo getMemberVo(MemberInfo memberInfo);
}
