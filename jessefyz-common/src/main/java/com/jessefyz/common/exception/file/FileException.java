package com.jessefyz.common.exception.file;

import com.jessefyz.common.exception.BaseException;

/**
 * 文件信息异常类
 * 
 * @author jessefyz
 */
public class FileException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public FileException(String code, Object[] args)
    {
        super("file", code, args, null);
    }

}
