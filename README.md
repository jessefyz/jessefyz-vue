# jessefyz-vue

#### 介绍
基于若依前后端分离版做了比较大的调整，适合从事软件定制的兄弟，减轻繁琐的项目初始化工作，优化代码结构，添加api模块用于为其他端（小程序，H5，PC官网，APP等）提供接口
！支持docker部署，更换了swagger，使用knife4j,界面更加清晰
#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/174935_d5dfa223_924029.png "屏幕截图.png")


#### 安装教程

1.  使用idea将AdminApplication和admin-ui跑起来
    账号：admin   密码：admin123
2.  后台admin接口文档地址：http://localhost:8080/admin/doc.html
3.  api有自己的鉴权体系，详细可以看com.jessefyz.module.interceptor.AuthInterceptor的鉴权代码

#### 使用说明
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180122_affce7fe_924029.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180143_d03719a1_924029.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180218_e8781ffe_924029.png "屏幕截图.png")

接口文档
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/180254_7bf25789_924029.png "屏幕截图.png")

#### 特别感谢

1.  若依前后端分离版本
2.  elasticsearch
3.  magic-api
4.  wxJava
